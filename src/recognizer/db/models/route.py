import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship

from ..database import Base


class Route(Base):
    __tablename__ = 'route'
    id = sa.Column(sa.Integer, name='id', primary_key=True, autoincrement=True)
    date = sa.Column(sa.Date, name='date')
    coords = sa.Column(postgresql.ARRAY(sa.Numeric), name='coords')

    defects = relationship('Defect', back_populates='route')
