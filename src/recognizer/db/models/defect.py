import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship

from db.enums import DefectType
from ..database import Base


class Defect(Base):
    __tablename__ = 'defect'
    id = sa.Column(sa.Integer, name='id', primary_key=True, autoincrement=True)
    is_actual = sa.Column(sa.Boolean, name='is_actual', default=False)
    type = sa.Column(sa.Enum(DefectType), name='type')
    photo = sa.Column(sa.String, name='photo', nullable=True)
    coords = sa.Column(postgresql.ARRAY(sa.Numeric), name='coords')
    date = sa.Column(sa.Date, name='date')
    route_id = sa.Column(sa.Integer, sa.ForeignKey('route.id', ondelete='SET NULL'))
    route = relationship('Route', back_populates='defects')
