from sqlalchemy.orm import Session

from db.models.defect import Defect
from core.exceptions import ObjectNotFound
from api.schemas.defect import DefectCreateSchema, DefectUpdateSchema


class DefectRepository:

    def get_defect_by_id(self, session: Session, id: int) -> Defect | None:
        """ Получение дефекта по id """
        return session.query(Defect).filter(Defect.id == id).first()

    def get_defect_by_id_or_404(self, session: Session, id: int) -> Defect:
        """ Получение дефекта по id. 404 ошибка, если он не найден """
        obj = self.get_defect_by_id(session, id)
        if not obj:
            raise ObjectNotFound
        return obj

    def get_defects_by_route_id(self, session: Session, route_id: int) -> list[Defect]:
        """ Получение списка маршрутов """
        return session.query(Defect).filter(Defect.route_id == route_id)

    def create_defect(self, session: Session, data: DefectCreateSchema) -> Defect:
        obj = Defect(**data.dict(exclude_unset=True))
        session.add(obj)
        session.commit()

        session.refresh(obj)

        return obj

    def update_defect(self, session: Session, id: int, data: DefectUpdateSchema) -> Defect:
        obj = self.get_defect_by_id_or_404(session, id)
        for field, value in data.dict(exclude_unset=True).items():
            setattr(obj, field, value)

        session.commit()
        session.refresh(obj)

        return obj
