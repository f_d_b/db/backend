from sqlalchemy.orm import Session, joinedload

from db.models.route import Route
from core.exceptions import ObjectNotFound
from api.schemas.route import RouteCreateSchema


class RouteRepository:

    def get_route_by_id(self, session: Session, id: int) -> Route | None:
        """ Получение маршрута по id """
        return session.query(Route).filter(Route.id == id).options(joinedload('defects')).first()

    def get_route_by_id_or_404(self, session: Session, id: int) -> Route:
        """ Получение маршрута по id. 404 ошибка, если он не найден """
        obj = self.get_route_by_id(session, id)
        if not obj:
            raise ObjectNotFound
        return obj

    def get_routes(self, session: Session) -> list[Route]:
        """ Получение списка маршрутов """
        return session.query(Route).all()

    def create_route(self, session: Session, data: RouteCreateSchema) -> Route:
        obj = Route(**data.dict(exclude_unset=True))
        session.add(obj)
        session.commit()

        session.refresh(obj)

        return obj
