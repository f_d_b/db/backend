from enum import auto

from strenum import StrEnum


class DefectType(StrEnum):
    pit = auto()
    crack = auto()
