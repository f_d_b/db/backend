import datetime
from pydantic import BaseModel, validator

from core.exceptions import CoordValueError, CoordsError
from db.enums import DefectType


class DefectBaseSchema(BaseModel):
    photo: str | None = None
    type: DefectType
    date: datetime.date
    route_id: int

    class Config:
        orm_mode = True


class DefectShortSchema(BaseModel):
    id: int
    coords: list[float, float]

    class Config:
        orm_mode = True


class DefectGetSchema(DefectBaseSchema):
    id: int
    is_actual: bool
    coords: list[float, float]


class DefectUpdateSchema(BaseModel):
    is_actual: bool | None = None
    photo: str | None = None
    type: DefectType | None = None
    date: datetime.date | None = None
    route_id: int | None = None

    class Config:
        orm_mode = True


class DefectCreateSchema(DefectBaseSchema):
    coords: str

    @validator('coords')
    def validate_coords(cls, v):
        if v is not None:
            try:
                items = [float(item) for item in v.split(',')]
            except ValueError:
                raise CoordValueError
            if len(items) != 2:
                raise CoordsError
            return items
        return v
