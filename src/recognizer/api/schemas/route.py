import datetime

from pydantic import BaseModel, validator

from core.exceptions import CoordValueError, CoordsError
from .defect import DefectGetSchema, DefectShortSchema


class RouteGetSchema(BaseModel):
    id: int
    coords: list[list[float, float]]
    date: datetime.date
    defects: list[DefectGetSchema]

    def __init__(self, **data):
        data['start_coords'] = data['coords'][0]
        data['end_coords'] = data['coords'][-1]
        super().__init__(**data)

    class Config:
        orm_mode = True


class RouteListSchema(RouteGetSchema):
    defects: list[DefectShortSchema]


class RouteCreateSchema(BaseModel):
    coords: str
    date: datetime.date

    @validator('coords')
    def validate_coords(cls, v):
        if v is not None:
            try:
                items = [[float(item.strip()) for item in coords.split(',')] for coords in v.split(';')]
            except ValueError:
                raise CoordValueError
            if any(len(coords) != 2 for coords in items):
                raise CoordsError
            return items
        return v


class PhotoSchema(BaseModel):
    path: str
