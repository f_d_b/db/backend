from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

from sqlalchemy.orm import Session

from api.schemas.defect import DefectGetSchema, DefectCreateSchema, DefectUpdateSchema
from db.crud.defect import DefectRepository
from db.database import get_session


router = InferringRouter()


@cbv(router)
class RouteRoute:
    model = None

    session: Session = Depends(get_session)
    repository: DefectRepository = Depends(DefectRepository)

    @router.get('/{id}')
    def retrieve(self, id: int) -> DefectGetSchema:
        """ Метод для получения информации о дефекте """
        return self.repository.get_defect_by_id_or_404(self.session, id)

    @router.get('/by_route/{route_id}')
    def list(self, route_id: int) -> list[DefectGetSchema]:
        """ Метод для получения информации о всех дефектах маршрута """
        return self.repository.get_defects_by_route_id(self.session, route_id)

    @router.post('/')
    def create(self, data: DefectCreateSchema) -> DefectGetSchema:
        return self.repository.create_defect(self.session, data)

    @router.patch('/{id}')
    def update(self, id: int, data: DefectUpdateSchema) -> DefectGetSchema:
        return self.repository.update_defect(self.session, id, data)
