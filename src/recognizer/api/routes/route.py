import os

from fastapi import Depends, UploadFile
from fastapi.responses import FileResponse
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from PIL import Image
from sqlalchemy.orm import Session

from api.schemas.route import RouteGetSchema, RouteListSchema, RouteCreateSchema, PhotoSchema
from core.exceptions import PhotoNotFound
from db.crud.route import RouteRepository
from db.database import get_session


router = InferringRouter()


@cbv(router)
class RouteRoute:
    model = None

    session: Session = Depends(get_session)
    repository: RouteRepository = Depends(RouteRepository)

    @router.post('/upload_photo/{id}', tags=['photos'])
    def upload_photo(self, id: int, file: UploadFile) -> PhotoSchema:
        route = self.repository.get_route_by_id_or_404(self.session, id)
        route_path = f'../../tmp/{route.id}'
        file_path = f'{route_path}/{file.filename}'
        os.makedirs(route_path, 0o777, exist_ok=True)

        photo = Image.open(file.file)
        photo.save(file_path, optimize=True)
        return PhotoSchema(path=file_path)

    @router.get('/get_photo', response_class=FileResponse, tags=['photos'])
    def get_photo(self, file_path: str):
        """ Получение фото по названию файла """
        path = f'{file_path}'
        if not os.path.exists(path):
            raise PhotoNotFound
        return path

    @router.get('/{id}')
    def retrieve(self, id: int) -> RouteGetSchema:
        """ Метод для получения информации о маршруте """
        return self.repository.get_route_by_id_or_404(self.session, id)

    @router.get('/')
    def list(self) -> list[RouteListSchema]:
        """ Метод для получения информации о всех  маршрутах """
        return self.repository.get_routes(self.session)

    @router.post('/')
    def create(self, data: RouteCreateSchema) -> RouteGetSchema:
        return self.repository.create_route(self.session, data)
