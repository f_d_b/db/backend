from pydantic import Field, BaseSettings as PydanticBaseSettings


class BaseSettings(PydanticBaseSettings):
    class Config:
        case_sensitive = False


class UvicornSettings(BaseSettings):
    HOST: str = Field('0.0.0.0', env='HOST')
    PORT: int = Field(8000, env='PORT')
    RELOAD: bool = Field(True, env='RELOAD')


class DBSettings(BaseSettings):
    HOST: str = Field('localhost', env='DB_HOST')
    PORT: str = Field('5433', env='DB_PORT')
    NAME: str = Field('recognizer', env='DB_NAME')
    USER: str = Field('postgres', env='DB_USER')
    PASSWORD: str = Field('postgres', env='DB_PASSWORD')

    @property
    def database_url(self) -> str:
        return f'postgresql+psycopg2://{self.USER}:{self.PASSWORD}@{self.HOST}:{self.PORT}/{self.NAME}'


class Settings(BaseSettings):
    db: DBSettings = DBSettings()
    uvicorn: UvicornSettings = UvicornSettings()

    LOG_FILE_PATH: str = Field('../../logs/logs.log', env='LOG_FILE_PATH')
    LOG_LEVEL: str = Field('debug', env='LOG_LEVEL')


settings = Settings()
