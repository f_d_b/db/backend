from .base import BaseAPIException


class PhotoNotFound(BaseAPIException):
    """ Фото не найдено """
    status_code = 404
    code = 'photo_not_found'
    message = 'Фото не найдено'
