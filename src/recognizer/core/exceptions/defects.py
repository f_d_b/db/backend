from .base import BaseAPIException


class CoordsError(BaseAPIException):
    """ Неверное количство координат """
    status_code = 400
    code = 'coords_error'
    message = 'Должно быть передано две координаты'


class CoordValueError(BaseAPIException):
    """ Неверное значение координат """
    status_code = 400
    code = 'not_float_coord'
    message = 'Координаты должны иметь тип float'
