from fastapi import FastAPI
import uvicorn

from api.routes.route import router as route_router
from api.routes.defect import router as defect_router
from config.settings import settings
from core.exceptions import BaseAPIException, exception_handler, python_exception_handler


def get_app():
    app = FastAPI()
    app.exception_handler(BaseAPIException)(exception_handler)
    app.exception_handler(Exception)(python_exception_handler)
    app.include_router(route_router, prefix='/api/v1/route', tags=['route'])
    app.include_router(defect_router, prefix='/api/v1/defect', tags=['defect'])

    return app


if __name__ == '__main__':
    uvicorn.run(
        'asgi:app',
        reload=settings.uvicorn.RELOAD,
        host=settings.uvicorn.HOST,
        port=settings.uvicorn.PORT,
        log_level=settings.LOG_LEVEL,
        http='h11',
        loop='asyncio',
    )
