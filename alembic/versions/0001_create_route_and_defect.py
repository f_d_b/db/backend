"""create_route_and_defect

Revision ID: 0001
Revises: 
Create Date: 2023-09-09 03:10:43.517297

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision: str = '0001'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('route',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('coords', postgresql.ARRAY(sa.Numeric()), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('defect',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('is_actual', sa.Boolean(), nullable=True),
    sa.Column('type', sa.Enum('pit', 'crack', name='defecttype'), nullable=True),
    sa.Column('photo', sa.String(), nullable=True),
    sa.Column('coords', postgresql.ARRAY(sa.Numeric()), nullable=True),
    sa.Column('date', sa.Date(), nullable=True),
    sa.Column('route_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['route_id'], ['route.id'], ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('defect')
    op.drop_table('route')
    # ### end Alembic commands ###
