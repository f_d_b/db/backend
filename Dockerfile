FROM python:3.10

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /code/
WORKDIR /code/

RUN mkdir logs

ENV PYTHONPATH "/code/src/recognizer"

RUN chmod a+x scripts/*.sh

CMD bash scripts/app.sh