#!/bin/bash

alembic upgrade head

cd src/recognizer

gunicorn "asgi:app" --bind 0.0.0.0:8080 --reload --workers 4 --worker-class uvicorn.workers.UvicornWorker